FROM node:latest as BUILDER
WORKDIR /app
COPY package*.json ./
RUN npm ci
ADD . .
RUN npm run build

FROM nginx:alpine
RUN mkdir /app
COPY --from=BUILDER /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf